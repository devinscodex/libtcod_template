/*
 * Codex
 * 2018 dev.d
 *
 * Boilerplate console code,
 * not just for roguelikes...
 */
#ifndef CODEX_HPP
#define CODEX_HPP

#define CODEX_TITLE "Codex"
#define CODEX_VERSION "1.0"
#define CODEX_STATUS "Pre-Alpha"

#include "libtcod.hpp"


#endif